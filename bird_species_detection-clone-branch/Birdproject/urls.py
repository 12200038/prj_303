
from django.contrib import admin
from django.urls import path
from Birdapp import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home, name='home'),
    path('register/',views.register,name='register'),
    path('login/',views.user_login,name='login'),
    path('userhome/',views.userhome,name='userhome'),
    path('logout/', views.logout_view, name='logout'),


]
