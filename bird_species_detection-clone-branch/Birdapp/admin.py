from django.contrib import admin
from .models import Feedback,BirdRecord
# Register your models here.
admin.site.register(Feedback)
admin.site.register(BirdRecord)