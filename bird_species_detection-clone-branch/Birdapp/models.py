from django.db import models

class Feedback(models.Model):
    user=models.CharField(max_length=15)
    name=models.CharField(max_length=15)
    email=models.EmailField()
    subject = models.CharField(max_length=255)
    message = models.TextField()
    def __str__(self):
        return self.subject

class BirdRecord(models.Model):
    image = models.ImageField(upload_to='bird_images')
    date = models.DateField()
    bird_name = models.CharField(max_length=255)
    bird_detail = models.TextField()
    def __str__(self):
        return self.bird_name
    