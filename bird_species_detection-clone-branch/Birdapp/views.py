from django.shortcuts import render ,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
from .forms import FeedbackForm,ImageForm
from .models import Feedback
from django.http import JsonResponse
import tensorflow as tf
from tensorflow import keras

def home(request):
    return render(request, 'home.html')

@login_required
def userhome(request):
    if request.method == 'POST':
        if 'feedback' in request.POST:  # handle prediction form
            form = FeedbackForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                email = form.cleaned_data['email']
                subject = form.cleaned_data['subject']
                message = form.cleaned_data['message']
                user = request.user
                feedback = Feedback(user=user, name=name, email=email,subject=subject, message=message)
                feedback.save()
                return redirect('userhome')
        else:
            form = FeedbackForm(initial={'name': request.user.username, 'email': request.user.email})
        return render(request, 'userhome.html', {'form': form})
    elif 'predict' in request.POST:  # handle prediction form
            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                # Get the image from the form data
                image = form.cleaned_data['image']

                # Load the machine learning model
                model = keras.models.load_model('/models/bird_vgg.h5')

                # Preprocess the image
                # ...

                # Make a prediction using the machine learning model
                predicted_species = model.predict(image)

                # Return the prediction to the user as JSON data
                return JsonResponse({'predicted_bird_species': predicted_species})

@csrf_exempt
def register(request):
    if request.method == 'POST':
        uname = request.POST.get('username')
        email = request.POST.get('email')
        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')
        
        # validate username
        if not uname:
            error_message = "Please enter a username."
        elif User.objects.filter(username=uname).exists():
            error_message = "Username already exists."
        elif len(uname) < 3:
            error_message = "Username must be at least 3 characters long."
        elif len(uname) > 30:
            error_message = "Username can be at most 30 characters long."
    
        else:
            # validate email
            if not email:
                error_message = "Please enter an email."
            elif User.objects.filter(email=email).exists():
                error_message = "Email already exists."
           
            else:
                # validate password
                if not pass1:
                    error_message = "Please enter a password."
                elif pass1 != pass2:
                    error_message = "Password and confirm password do not match."
                else:
                    # create user
                    try:
                        my_user = User.objects.create_user(email=email, password=pass1, username=uname)
                        my_user.save()
                        return redirect('login')
                    except IntegrityError:
                        error_message = "An error occurred while creating your account. Please try again later."
        
        context = {
            'error_message': error_message,
            'uname': uname,
            'email': email
        }
        return render(request, 'register.html', context)

    return render(request, 'register.html')

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('pass')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('userhome')
        else:
            context = {'error': 'Username or Password is incorrect!!!'}
            return render(request, 'login.html', context)

    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('home')

def my_view(request):
    context = {'user': request.user}
    return render(request, 'userhome.html', context)

